require = patchRequire global.require;
casper = require('casper').create
  timeout: 15000
  waitTimeout: 5000
  pageSettings:
    loadImages: false
    loadPlugins: true

params = JSON.parse casper.cli.get 0

casper.userAgent 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36'

casper.params = params

casper.report = (data) ->
  @echo JSON.stringify data, null, '\t'

module.exports = casper

