casper = require './casper.helper'
#casper.enableDebug()
fs = require 'fs'
params = casper.params
data = params.data
matches =
  first: ['first', 'name'],
  last: ['last', 'name'],
  mail: ['mail'],
  phone: ['phone', 'number'],
  inquiry: ['inquiry', 'subject'],
  country: ['country'],
  city: ['city'],
  zip: ['zip', 'postal'],
  address: ['address'],
  state: ['state'],
  text: ['message', 'comment', 'suggestion', 'text', 'question', 'feedback'],
  captcha: ['captcha', 'verification']

mails = []
forms = []
form = ''
fields = {}
links = []
labels = []
texts = []
inputs = []

xpath = (path) ->
  type: 'xpath'
  path: path

get_elements = (path) ->
  path = xpath path
  if casper.exists path
    return casper.getElementsInfo path
  else
    return []

get_one = (path) ->
  path = xpath path
  if casper.exists path
    return casper.getElementInfo path

errors = []
error = (message, path) ->
  errors.push("#{message} for #{path} on #{casper.getCurrentUrl()}") # at #{new Date()}")

isContactForm = (form)->
  form_path = switch
    when form.id then "//form[@id='#{form.id}']"
    when form.name then "//form[@name='#{form.name}']"
    else "//form"

  find = (what, where) ->
    what = what.toLowerCase()
    for word in where
      if what.indexOf(word) >= 0
        return true
    return false

  textareas = get_elements form_path + '//textarea'
  if 0 == textareas.length
    return false

  for label in get_elements form_path + '//label'
    if find label['html'], matches.text
      form = form_path
      return true
  for textarea in textareas
    if find textarea.attributes.value, matches.text
      if textareas.length > 0
        form = form_path
        return true
  return false


findMails = ->
  html = casper.getHTML()
  regexp = /([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/g
  #regexp = /[^\s]+@[^\s]+\.[^\s+]/g
  while tmp = regexp.exec html
    mails.push tmp
  return mails.length > 0

findForms = ->
  forms = get_elements 'form'
  for f in forms
    if isContactForm f.attributes
      return findFormFields()
  return false


findFormFields = ()->
  textareas = get_elements '//textarea'
  if textareas.length == 0
    return false
  labels = get_elements "//label"
  inputs = get_elements "//input[@type!=hidden]"
  selects = get_elements "//select"
  hiddens = get_elements "//input[@type=hidden]"
  for hidden in hiddens
    if hidden.attributes.name
      fields["input[@name=#{hidden.attributes.name}"] = hidden.attributes
  for category, match of matches
    field = ($input, input, $label) ->
      if not input or $input in cats
        return false
      attrs = input.attributes
      attrs.$cat = category
      if $label
        label_parent = get_one $label + '/..'
        if '*' in label_parent.html
          attrs.$required = true
      if attrs['aria-required'] or attrs.required in ['true', 'requred']
        attrs.$required = true
      fields[$input] = attrs
      return true

    for variant in match

      for i, label of labels
        text = label['html'].toLowerCase()
        if text.indexOf(variant) >= 0
          texts.push text
          if 'for' in label.attributes
            id = label.attributes.for
            input_xpath = "[@id=#{id}]"
            input = get_elements input_xpath
            if input.length > 0
              if input.length > 1
                error "More than one input with id #{id} found", input_xpath
                input_xpath =+ "[position()=1]"
              field(input_xpath, input)
#              return true
            else
              error "No one input with id #{id} found", input_xpath
#              return false
          else
            label_xpath = "//label[position()=#{i}]"
            label = get_elements label_xpath
            if label.length > 1
                error 'More than one input in label found', label_xpath
            input_xpath = label_xpath + '/following-sibling::input'
            field(input_xpath, get_one(input_xpath), label_xpath)

      for i, input of inputs
        attrs = input.attributes
        if attrs.name
          text = attrs.name.toLowerCase()
          if text.indexOf(variant) >= 0
            input_xpath = "//input[@name='#{attrs.name}']"
            _input = get_elements input_xpath
            if _input.length > 0
              error "More than one input with name #{attrs.name} found", input_xpath
              _input = _input[0]
            else if 0 == _input.length
              error "No one input with name #{attrs.name} found", input_xpath
            field(input_xpath, _input)

  if textareas.length > 1
    error "More than one input with id #{id} found", '//textarea'
  name = textareas[0].attributes.name
  field("//textarea[@name='#{name}']", textareas[0])
  return true


fillForm = ->
  values = {}
  keys = Object.keys params
  for path, field of fields
    if field.$cat in keys
      values[path] = params[field.$cat]
  casper.fillXPath form, values, true

getLinks = ->
  curl = casper.getCurrentUrl()
  words = ['contact', 'about']
  if casper.exists 'a'
    anchors = casper.getElementsInfo 'a'
    for a in anchors
      link = a['html'].toLowerCase()
      for text in words
        if link.indexOf(text) >= 0
          links.push a['html']
          break

report = (status, data)->
  status: status
  data: data
  errors: errors
#  labels: labels
#  inputs: inputs
  fields: fields
  texts: texts

contactForm = ->
  if findMails()
    casper.then ->
      casper.report report 'mail',
        mails: mails
  else if findFormFields()
    fillForm fields
    casper.then ->
      casper.report report 'form'
  else if links.length
    casper.then ->
      casper.clickLabel links.pop(), 'a'
    casper.then ->
      contactForm()
  else
    casper.then ->
      casper.report report 'none'

timeout = () ->
  casper.report status: 'timeout'

casper.start params.url, ->
  getLinks()
  casper.then ->
    casper.waitForSelector 'input', contactForm, timeout, 5000



casper.run()

