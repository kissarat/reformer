fs = require 'fs'
path = require 'path'
childProcess = require 'child_process'
phantomjs = require 'phantomjs'
#just a workaround to get casperjs location in local node_modules
binPath = phantomjs.path.replace 'phantomjs/lib/phantom/bin/phantomjs', 'casperjs/bin/casperjs'

class main
  constructor: ->
    @params  = {}
  init: (url,data)->
    @params.url = url
    @params.data = require '../'+data
    @run()

  run: ->
    process.env.PHANTOMJS_EXECUTABLE = phantomjs.path

    childArgs = [
      path.join(__dirname, 'casperscripts', "run.coffee"),
      JSON.stringify @params
    ]
    childArgs.push "--ignore-ssl-errors=true"
    childProcess.execFile binPath, childArgs, (err, stdout, stderr) =>
      console.log err if err
      console.log stderr if stderr
      console.log stdout

module.exports = new main()